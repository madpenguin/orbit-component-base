# Orbit Component :: Base : Server

#### This is the base compoent for the orbit framework back-end. It's required for all other components and for the framework itself, assuming you want the framework to do something useful.

##### <b>Note</b>: Please see the project source repository for more information