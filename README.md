# Orbit Component :: Base

### Note:

As a result of changes to PyPi Terms, this package is no longer available from the standard PyPi server. In order to use
this package with "pip" either add "-i https://pypi.madpenguin.uk" to your command line, or for transparency;
```bash
# Create or add to ~/.config/pip/pip.conf
[global]
extra-index-url = https://pypi.madpenguin.uk
```

#### This repository contains an Orbit Framework component which forms the base for all other components. In order to use the component you will need to install the contents of the server folder (on the server) ideally via 'pip', and the contents of the client folder (in the server) ideally via npm. Please make sure the versions of the client and server as in sync, using different versions of the client and server together is likely to cause problems.

### How to use the Server

Once you have installed the server component into your project, it will automatically be picked up and used by your Orbit application. It will provide two things to your application, namely;

* A number of additional command line arguments available when you launch your application
* A number of additional RPC endpoints that will be available 'to' your application

After installing the package, if you run your application with "--help" you should see (at least) two new flags available, "--dev" and "--run".

For more information, please see the Orbit Framework Tutotial / getting started.

### Components - under the hood

When an Orbit application starts up it will look for available packages that begin with the name "orbit_component". It will then try to initialise each matching package as a component by using it's "plugin.py/Plugin" class. This will initialise any specified database tables and add new command line arguments and RPC endpoints.

### Component websocket connections

The base module will connect (by default) ONE connection to it's default server and initial the "/orbit" namespace. Each subsequently loaded component will then open up it's own namespace, but it will be multiplexed via the main connection. So although it might appear that each component has it's own connection, actually it has it's own namespace and everything happens over a single connection. i.e. there is no major network resource implication arising from loading lots of components.