# Orbit Component :: Base : Client

#### This code forms the client component of an Orbit Framework application. Include this into your applicaiton to make it an Orbit application which will wrap access to websockets, data models, namespace partitioning and reactive feedback.

##### <b>Note</b>: Please see the project source repository for more information