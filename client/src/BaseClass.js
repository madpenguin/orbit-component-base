import { ref } from 'vue';

const difference = (a, b) => new Set([...a].filter(x => !b.has(x)))

function OrmBase (context) {
    let self = context.store
    if (!context.options.useOrmPlugin) {
        return
    }
    if (!self.model) throw new Error('no model has been set for this class')
    self._vm = null
    self._sockets = new Map()
    self.data = new Map()
    self.local_ids = new Map()
    self.local_indexes = {}
    self.local_lookups = new Map()
    self.local_params = {}
    self.active = ref(false)
    self.ids = (label) => { return self.local_ids.get(label)||[] }

    self.init = function (base, component, socket) {
        // console.log(`Socket connected 2.0 => ${self.model} => ${socket.connected} => ${socket._opts.host}`)
        if (socket) {
            self._vm = base
            self._sockets.set(component, socket)
            self._component = component
            if (self.hook_init) self.hook_init(base)
            socket.on(`${self.model}__invalidate`, (response) => {
                self.invalidate(response)
            })
            self.active = true
        } else {
            console.log(`[ERROR] null socket => ${self.model}`)
        }
        return self
    }

    self.invalidate = function (response) {
        if (self.hook_invalidate) self.hook_invalidate(response)
        response.data.forEach(id => {if (id in self.data) self.data.delete(id)})
    }

    self.drop = function (params) {
        const socket = self._sockets.get(params.component)
        if (!socket) throw 'socket not initialised'
        // #FIXME: drop
        socket.emit('drop_nql', params, (response) => {
            if (!response||!response.ok) throw new Error(`Socket error: ${response.toString()}`)
            self.local_ids.set(params.label, [])
            self.invalidate(response)
        })
    }

    self.query = function (params, onComplete) {
        const socket = self._sockets.get(params.component)
        if (!socket) throw 'socket not initialised'
        if (!socket.connected) throw `socket not connected, model=${self.model}`
        // #TODO: query
        self.subscribe_ids(socket, params)
        socket.emit('call_nql', params, (response) => {
            if (response && response.ok && response.ids) {
                self.local_params[params.label] = params
                self.store_response(params, response, onComplete)
            } else {
                console.warn('call failed :: ', response)
                if(onComplete)onComplete(response)
            }
        })
    }

    self.subscribe_ids = function (socket, params) {
        if (!(self.local_ids.has(params.label))) {
            self.local_ids.set(params.label, [])
            socket.on(`${params.model}_${params.label}`, (response) => {
                console.log("Received: ", params.model, "|", params.label, "|", response)
                response.update = true
                self.store_response(params, response)
            })
        }
    }

    self.store_response = function (params, response, onComplete=null) {
        if(self.hook_store_pre) self.hook_store_pre(params)
        const stamp = Math.floor(new Date().getTime()/1000)
        if(self.local_lookups.size)self.store_lookup(params, response)
        if (response.data) {
            response.data.forEach(item => {
                if (item._id in self.data) item.timestamp = stamp
                self.data.set(item._id, item)
            })
        }
        response.params = params
        let promises = []
        let chain = params.chain || []
        let r = {...response}
        r.next = true
        r.context = self
        chain.forEach(fn => promises.push(fn(r)))
        Promise.all(promises).then((values) => {
            values.forEach(fn => {if(fn)fn()})
            if (response.ids) {
                if (params.next) {
                    self.local_ids.set(params.label, [...new Set([...self.local_ids[params.label],...response.ids])])
                } else {
                    self.local_ids.set(params.label, [...response.ids])
                }
            }
            if(self.sort) self.sort(params)
            if(self.hook_sort)self.hook_sort(params)
            if(Object.keys(self.local_indexes).length)self.store_index(response)
            // if(self.local_lookups.size)self.store_lookup(response)
            if(self.store_response_post) self.store_response_post(response)
            if(self.hook_store_post) self.hook_store_post(response)
            if(onComplete)onComplete(response)
            if(!response.update)self.local_params[params.label].next = false
        })
    }

    self.clear = function (model, label) {
        if (label in self.local_ids) {
            self.local_ids.set(label, [])
        }
    }

    self.reload = function (nsp) {
        for (let label in self.local_params) {
            let params = self.local_params[label]
            const already_have = self.local_ids.get(label).length
            if (already_have > params.count) params.count = already_have
            params.next = false
            const socket = self._sockets.get(params.component)
            if (!socket || socket.nsp != nsp) continue
            socket.emit('call_nql', params, (response) => {
                if (response && response.ok && response.ids) {
                    self.store_response(params, response)
                }
            })
        }
    }
    self.create_lookup = function (field, forward, reverse, fn=null) {
        self.local_lookups.set(field, {field: field, forward: forward, reverse: reverse, fn: fn})
    }
    self.store_lookup = function (params, response) {
        self.local_lookups.forEach(lookup => {
            if (response.ids) {
                // console.log("Local>", self.local_ids, params)
                const old_keys = new Set(self.local_ids.get(params.label))
                const new_keys = new Set(response.ids)
                // console.log("Old Keys>", old_keys)
                // console.log("New Keys>", new_keys)
                difference(old_keys, new_keys).forEach(id => {
                    const data = this.data.get(id)
                    if (data) {
                        const key = lookup.fn ? lookup.fn(data) : data[lookup.field]
                        // console.log("Del>", key)
                        lookup.reverse.delete(key)
                    }
                });
            }
            (response.data||[]).forEach(data => {
                const orig = this.data.get(data._id)
                if (orig) {
                    const okey = lookup.fn ? lookup.fn(orig) : orig[lookup.field]
                    if (okey) lookup.reverse.delete(okey)
                }
                const nkey = lookup.fn ? lookup.fn(data) : data[lookup.field]
                if (nkey) lookup.reverse.set(nkey, data)
            })
        })
    }

    self.create_index = function (field, forward, reverse) {
        self.local_indexes[field] = {field: field, forward: forward, reverse: reverse}
    }
    self.store_index = function (response) {
        Object.values(self.local_indexes).forEach(index => {
            const old_keys = new Set(Object.keys(index.reverse))
            const new_keys = new Set(response.ids)
            difference(old_keys, new_keys).forEach(id => {
                if (index.reverse[id][index.field].map) {
                    index.reverse[id][index.field].forEach(address => {
                        delete index.forward[address]
                    })
                } else {
                    const address = index.reverse[id][index.field]
                    delete index.forward[address]
                }
                delete index.reverse[id]
            });
            (response.data||[]).forEach(data => {
                if (index.field in data) {
                    if(data[index.field].map) {
                        data[index.field].forEach(entry => {
                            if (!(entry.address in index.forward)) {
                                index.forward[entry.address] = data
                            }
                        })
                    } else {
                        const entry = data[index.field]
                        if (!(entry.address in index.forward)) {
                            index.forward[entry.address] = data
                        }
                    }
                    index.reverse[data._id] = data
                }
            })
        })
    }
    self.lookup_forward = function (index_name, key) {
        const index = self.local_indexes[index_name]
        return key in index.forward ? index.forward[key] : null
    }
    self.lookup_reverse = function (index_name, id) {
        const index = self.local_indexes[index_name]
        return id in index.reverse ? index.reverse[id] : null
    }
    if (!self.sort && !self.hook_sort) {
        try {
            self.sort = function (params) {
                if (self.local_ids.has(params.label)) {
                    self.local_ids.get(params.label).sort((a,b) => {
                        try {
                            const aa = self.data[a]
                            const bb = self.data[b]
                            if (aa.name < bb.name) return -1
                            if (aa.name > bb.name) return 1
                            return 0
                        } catch (error) {
                            console.log(`ERROR: model=${params.model} label=${params.label} no data for: "${a}" or "${b}" ${error}`)
                        }
                    })
                }
            }
        }
        catch (err) {
            console.log("Caught Error: err")
            console.log(`model=${model} label=${label}`)
        }
    }
    return self
}

export default OrmBase
