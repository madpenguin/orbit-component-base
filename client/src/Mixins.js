import pkg from '../package.json';

export default (name, defineStore) => {
    const install = (param_app, param_options, vm, app, opt) => {
        function translate(key) {      
            return key.split(".").reduce((o, i) => {        
                if (o) return o[i];      
            }, param_options);    
        }
        param_app.config.globalProperties.$translate = translate;
        param_app.component(name, vm);
        (param_options.buttons||[]).forEach((button) => {
            if (button.path) {
                // console.log("AddRoute>", button)
                param_options.router.addRoute(button)
            }
            param_options.menu.push(button)
        })
        Object.assign(app, param_app)
        Object.assign(opt, param_options)
    }
    const bootstrap = (version) => {
        // console.log("<< Adding ", name, version, ">>")
        defineStore('componentsStore')().add(name, {version: version})
    }
    const component_name = () => {
        return name
    }
    return {install, bootstrap, component_name};
}
