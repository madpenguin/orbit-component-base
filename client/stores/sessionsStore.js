import { defineStore } from 'pinia'

export const useSessionsStore = defineStore('sessionsStore', {
    state: () => {
        return {
            model: 'sessions'
        }
    },
    actions: {
        sort (params) {},
        populate (label, callback=null) {
            const method = `api_${this.model}_get_ids`
            this.query({
                model: this.model,
                label: label,
                method: method,
                component: label,
                filter: {
                    index_name: 'by_when',
                    reverse: true
                }
            },
                (response) => {
                    if (!response || !response.ok) throw new Error(response ? response.toString() : `no query: ${method}`)
                    if (callback) callback(response)                   
                }
            )
            return this
        }
    },
    useOrmPlugin: true
})