import { defineStore } from 'pinia'

export const usePermissionsStore = defineStore('permissionsStore', {
    state: () => {
        return {
            model: 'group_permissions',
            key_forward: new Map(),
            key_reverse: new Map(),
        }
    },
    actions: {
        hook_init (self) {
            this.create_lookup(
                'key',
                this.key_forward,
                this.key_reverse,
                (d) => `${d.name}|${d.exec}`.toLowerCase()
            )
        },
        sort (params) {},
        populate (label, callback=null) {
            const method = `api_${this.model}_get_ids`;
            this.query({
                model: this.model,
                label: label,
                method: method,
                component: label,
            },
                (response) => {
                    if (!response || !response.ok) throw new Error(response ? response.error : `no query: ${method}`)
                    if (callback) callback(response)                   
                }
            )
            return this
        },
        qresponse (response) {
            if (!response||!response.ok) console.log('Operation Failed: ', response)
            else if (response.params.callback) response.params.callback(response)
        },
        async load_own_details (response) {
            let data = null
            if (response.data && response.data.length == 1) {
                data = response.data[0]
            } else if (response.ids && response.ids.length == 1) {
                data = this.data.get(response.ids[0])
            } else {
                throw new Error('failed to derive incoming ID')
            }
            return await new Promise((resolve, reject) => {
                const params = {
                    model: this.model,
                    method: `api_${this.model}_get_ids_by_group`,
                    component: response.params.label,
                    label: response.params.label,
                    ids: (data && data.groups) || []
                }
                this.query(params, (response) => resolve(() => {
                    this.qresponse(response)
                }))
            })
        },
        checkAccess (endpoint, exec='default') {
            return this.key_reverse.has(`${endpoint}|${exec}`)
        }
    },
    useOrmPlugin: true
})