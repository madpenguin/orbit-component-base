import { defineStore } from 'pinia'
import { usePermissionsStore } from './permissionsStore';

export const useUsersStore = defineStore('usersStore', {
    state: () => {
        return {
            model: 'users',
            permissionsStore: null,
        }
    },
    actions: {
        sort (params) {},
        hook_init (self) {
            this.permissionsStore = usePermissionsStore()
        },
        populate (label, callback=null) {
            const method = `api_${this.model}_get_ids`
            this.query({
                model: this.model,
                label: label,
                component: label,
                method: method},
                (response) => {
                    if (!response || !response.ok) throw new Error(response ? response.toString() : `no query: ${method}`)
                    if (callback) callback(response)                    
                }
            )
            return this
        },
        // load_self (label, hostid, callback=null) {
        //     const method = `api_${this.model}_get_ids`
        //     this.query({
        //         component: label, 
        //         model: this.model,
        //         label: 'self',
        //         ids: [hostid],
        //         chain: [this.permissionsStore.load_self],
        //         method: method},
        //         (response) => {
        //             if (!response || !response.ok) throw new Error(response ? response.error : `no query: ${method}`)
        //             if (callback) callback(response)                
        //             // console.log("User self>", response)    
        //         }
        //     )
        // },
        load_own_details (root, hostid, callback=null) {
            const method = `api_${this.model}_get_own_details`
            this.query({
                component: root,
                model: this.model,
                label: root,
                hostid: hostid,
                method: method,
                chain: [this.permissionsStore.load_own_details]
            }, (response) => {
                // console.log(`[=> ${method}] `, response)
                if (!response || !response.ok) throw new Error(response ? response.error : `no query: ${method}`)
                if (callback) callback(response)                
            })            
        },
        checkAccess (namespace, hostid, endpoint, exec='default') {
            if (!this.data.has(hostid)) {
                console.warn(`!!${namespace}|${hostid}|${endpoint} > not loaded yet!`)
                return false
            }
            const user = this.data.get(hostid)
            const groups = user.groups || []
            if (groups && (groups.includes('admin') || groups.includes(namespace))) {
                return true
            }
            return this.permissionsStore.checkAccess(endpoint, exec)
        }
    },
    useOrmPlugin: true
})
