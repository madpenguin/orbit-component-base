import { fileURLToPath, URL } from 'node:url'
import path from 'path';
const { defineConfig } = require('vite');
const vue = require('@vitejs/plugin-vue');
const component = 'orbit-component-base'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'
import pkg from './package.json';
const external = Array.from(new Set([
  ...Object.keys(pkg.dependencies || {}),
  ...['vue', 'pinia', 'socket.io-client', 'uuid']
]))
console.log("External=", external)

export default defineConfig({
  build: {
    sourcemap: true,
    lib: {
      entry: path.resolve(__dirname, 'index.js'),
      name: component,
      fileName: (format) => `${component}.${format}.js`,
    },
    rollupOptions: {
      external: external,
      output: {
        format: "es",
        globals: {
          vue: 'vue',
          pinia: 'pinia',
          uuid: 'uuid',
          "socket.io-client": 'socket.io-client',
        }
      },
    }
  },
  plugins: [
    vue(),
    cssInjectedByJsPlugin()
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    },
    preserveSymlinks:true
  }  
});
